import React from 'react';
import cart  from '../cart';

class BookListItem extends React.Component<{ book: BookType }> {
    public render() {
        const {book} = this.props;

        return (
            <div className="book-list-item" key={book.isbn}>
                <div className="book-cover">
                    <img src={book.cover} alt={book.title}/>
                </div>

                <div className="book-title">{book.title}</div>

                <div className="book-price">{book.price}€</div>

                <div>
                    <button className="block" onClick={() => this._addToCart()}>Ajouter au panier</button>
                </div>
            </div>
        );
    }

    private _addToCart() {
        const {book} = this.props;

        cart.add({
            id: book.isbn,
            cover: book.cover,
            label: book.title,
            price: book.price
        });

        alert('Le livre a bien été ajouté au panier');
    }
}

export default BookListItem;