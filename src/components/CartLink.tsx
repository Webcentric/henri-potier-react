import React      from 'react';
import {Link}     from 'react-router-dom';
import {observer} from 'mobx-react';
import cart       from '../cart';

@observer
class CartLink extends React.Component {
    public render() {
        return (
            <Link to="/cart">Panier {cart.count ? `(${cart.count})` : ''}</Link>
        );
    }
}

export default CartLink;