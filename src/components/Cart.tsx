import React      from 'react';
import {observer} from 'mobx-react';
import {Link}     from 'react-router-dom';

import cart from '../cart';

cart.load();

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type OfferType = {
    type: 'percentage' | 'minus' | 'slice';
    value: number;
    sliceValue?: number;
}

@observer
class Cart extends React.Component {

    public render() {
        if (!cart.items.length) {
            return (
                <div>
                    <p>Votre panier ne contient aucun article.</p>

                    <Link to="/">Poursuivre vos achats</Link>
                </div>
            );
        }

        const discount = this.getDiscount();

        return (
            <div id="cart-page">
                <h1>Panier</h1>

                <hr/>

                <div className="row between">
                    <div className="col-lg col-md col-sm col-xs">
                        {cart.items.map(item => (
                            <div className="item">
                                <div>
                                    <img src={item.cover} alt={item.label}/>
                                </div>

                                <div className="item-info">
                                    <h2>{item.label}</h2>

                                    <div className="price">{item.price}€</div>

                                    {/*TODO Exercice 2 : Créer un composant pour changer la quantité*/}
                                    <input
                                        type="number"
                                        min="1"
                                        onChange={e => cart.setItemQuantity(item, parseInt(e.target.value))}
                                        value={item.quantity}
                                    />

                                    <div>
                                        <button onClick={() => cart.removeItem(item)}>
                                            Supprimer
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                    <div className="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <div className="total-container">
                            <div className="total-row">
                                <div>Sous-total</div>
                                <div>{cart.subTotal}€</div>
                            </div>

                            <div className="total-row">
                                <div>Remise</div>
                                <div>{discount}€</div>
                            </div>

                            <div className="total-row">
                                <div>Montant total</div>
                                <div>{cart.subTotal - discount}€</div>
                            </div>

                            <button className="block" onClick={() => alert('SUCCESS')}>Paiement</button>

                            <button className="block" onClick={() => cart.removeAll()}>Vider le panier</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    public getDiscount() {
        // TODO Exercice 3 : Compléter cette fonction pour afficher la remise sur le panier

        return 0;
    }
}

export default Cart;