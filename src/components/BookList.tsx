import BookListItem from './BookListItem';

function BookList() {
    // TODO Exercice 1 : Récupérer la liste des livres et l'afficher à la place du "livre d'exemple".

    return (
        <div>
            <h1>Liste des livres</h1>

            <hr/>

            <div className="row">
                <div className="col-lg-3 col-md-4 col-sm-4 col-xs">
                    <BookListItem book={{
                        isbn: 'default_isbn',
                        title: 'Livre d\'exemple',
                        price: 15,
                        cover: 'http://www.wcentric.com/henri-potier/imgs/hp1.jpg',
                    }}/>
                </div>
            </div>
        </div>
    );
}

export default BookList;