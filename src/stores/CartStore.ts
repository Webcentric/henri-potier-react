import {observable, makeAutoObservable} from 'mobx';

const CART_STORAGE_KEY = 'CART_STORAGE_KEY';

export type CartItemType = {
    id: string;
    label: string;
    cover: string;
    price: number;
    quantity: number;
};

class CartStore {

    private _items = observable<CartItemType>([]);

    constructor() {
        makeAutoObservable(this);
    }

    public add(item: Omit<CartItemType, 'quantity'>) {
        const cartItem = this._items.find(cartItem => cartItem.id === item.id);

        if (cartItem) {
            return this.setItemQuantity(cartItem, cartItem.quantity + 1);
        }

        this._items.push({...item, quantity: 1});

        return this._save();
    }

    public setItemQuantity(item: CartItemType, quantity: number) {
        const index = this._items.findIndex((cartItem) => item === cartItem);

        if (index >= 0) {
            this._items.splice(index, 1, {...item, quantity});
        }

        return this._save();
    };

    public removeItem(item: CartItemType) {
        this._items.remove(item);

        return this._save();
    };

    public removeAll() {
        this._items.clear();

        return this._save();
    };

    public get items() {
        return this._items;
    }

    public get count() {
        return this._items.reduce((count, item) => count + item.quantity, 0);
    }

    public load() {
        const str = localStorage.getItem(CART_STORAGE_KEY) || '[]';

        this._items = JSON.parse(str) || [];
    }

    public get subTotal() {
        return this._items.reduce((total, item) => total + (item.quantity * item.price), 0);
    }

    private _save() {
        localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(this._items));

        return this;
    }
}

export default CartStore;