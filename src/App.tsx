import './scss/App.scss';

import React from 'react';

import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';

import BookList from './components/BookList';
import Cart     from './components/Cart';
import CartLink from './components/CartLink';

function AppRouter() {
    return (
        <Router>
            <div className="App">
                <header className="header">
                    <h1 className="app-name"><a href="/">Henri Potier</a></h1>

                    <ul>
                        <li>
                            <CartLink/>
                        </li>
                    </ul>
                </header>

                <Switch>
                    <Route path="/" exact={true}>
                        <BookList/>
                    </Route>

                    <Route path="/cart">
                        <Cart/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default AppRouter;