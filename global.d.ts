declare type BookType = {
    isbn: string;
    title: string;
    price: number;
    cover: string;
}